package thegenuinegourav.voicemail.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import thegenuinegourav.voicemail.R;
import thegenuinegourav.voicemail.database.DatabaseOpenHelper;
import thegenuinegourav.voicemail.pojo.EmailMessages;
import thegenuinegourav.voicemail.utils.Utility;

import android.support.annotation.NonNull;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Message;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.people.v1.PeopleService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.people.v1.PeopleServiceScopes;
import com.google.api.services.people.v1.model.ListConnectionsResponse;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.PeopleFeed;
import com.google.api.services.plus.model.Person;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/*
 * TODO:This java class is useful to compose email for sending using speech to text and Gmail Apis.
 */
public class ComposeEmailActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private Button btn_submit;
    private TextToSpeech tts;
    private TextView status;
    private TextView To, Subject, Message;
    private int numberOfClicks;
    private boolean IsInitialVoiceFinshed;
    TextView name;
    SpeechRecognizer mSpeechRecognizer;
    Intent mSpeechRecognizerIntent;
    final int RC_INTENT = 200;
    final int RESOLVE_HINT = 300;
    final int RC_API_CHECK = 100;
    boolean isToClicked, isSubjectClicked, isMessageClicked;
    private GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    GoogleAccountCredential mCredential;
    Gmail mService;
    SharedPreferences sharedPref;
    Utility mUtils;
    public static final int REQUEST_AUTHORIZATION = 1001;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    boolean isSending = false;
    public static final String[] SCOPES = {
            GmailScopes.GMAIL_LABELS,
            GmailScopes.GMAIL_COMPOSE,
            GmailScopes.GMAIL_INSERT,
            GmailScopes.GMAIL_READONLY,
            GmailScopes.MAIL_GOOGLE_COM
    };
    public static final String PREF_ACCOUNT_NAME = "accountName";
    DatabaseOpenHelper databaseOpenHelper;
    int counter = 0;
    SwipeRefreshLayout refreshSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_email);

        refreshSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.refreshSwipeLayout);

        checkPermission();

        databaseOpenHelper = DatabaseOpenHelper.getInstance(ComposeEmailActivity.this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("78195453325-fgbvls3re3j02vat9rkeht5uqq91h2o3.apps.googleusercontent.com")//12500 error TODO:Add web client id and updated google-services.json
                .requestEmail()
                .requestServerAuthCode("78195453325-fgbvls3re3j02vat9rkeht5uqq91h2o3.apps.googleusercontent.com")
                .requestScopes(new Scope(PeopleServiceScopes.CONTACTS_READONLY),
                        new Scope(PeopleServiceScopes.USERINFO_EMAIL))
                .build();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this).addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        mService = null;
        sharedPref = ComposeEmailActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        mUtils = new Utility(ComposeEmailActivity.this);

        String accountName = sharedPref.getString(PREF_ACCOUNT_NAME, null);
        if (accountName != null) {
            mCredential.setSelectedAccountName(accountName);

            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new Gmail.Builder(
                    transport, jsonFactory, mCredential)
                    .setApplicationName("Voicemail")
                    .build();

        }

        IsInitialVoiceFinshed = false;

        //Text to speech declarations
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                    speak("Welcome to voice mail. Tell me the mail address to whom you want to send mail?");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            IsInitialVoiceFinshed = true;
                        }
                    }, 6000);
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });

        status = (TextView) findViewById(R.id.status);
        To = (TextView) findViewById(R.id.to);
        Subject = (TextView) findViewById(R.id.subject);
        Message = (TextView) findViewById(R.id.message);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail();
            }
        });
        setActionBar();


        //Speech to text declarations
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());

        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {

                //getting all the matches
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                //displaying the first match
                if (matches != null)
                    if (isToClicked) {
                        //To.setText(matches.get(0).replace(" ", ""));
                        String to;
                        to = matches.get(0).replace("underscore", "_").replace("dot", ".");
                        to = to.replaceAll("\\s+", "");
                        To.setText(to);
                        isToClicked = false;
                    }

                if (matches != null)
                    if (isSubjectClicked) {
                        Subject.setText(matches.get(0));
                        isSubjectClicked = false;
                    }

                if (matches != null)
                    if (isMessageClicked) {
                        Message.setText(matches.get(0));
                        isMessageClicked = false;

                        speak("You have composed this email to " + To.getText().toString().trim() + " with subject " + Subject.getText().toString().trim() + " and message " + Message.getText().toString().trim());

                        //System.out.println("REQUESTCODE222222---->>>>"+requestCode);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                speak("swipe finger down to proceed");
                            }
                        }, 6000);

                    }
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });

        btn_submit.setVisibility(View.GONE);


        //Swipe down method to get input from user
        refreshSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refreshSwipeLayout.setRefreshing(false);

                if (To.getText().toString().equalsIgnoreCase("Swipe down to add email address...")
                || mUtils.isValidEmail(To.getText().toString())==false) {

                    speak(" Whom you want to send mail?");
                    isToClicked = true;
                    isSubjectClicked = false;
                    isMessageClicked = false;

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                            To.setText("");
                            To.setHint("Listening...");

                        }
                    }, 1500);

                    return;
                }

                if (Subject.getText().toString().equalsIgnoreCase("Swipe down to add email subject...")) {
                    speak("What should be the subject?");
                    isToClicked = false;
                    isSubjectClicked = true;
                    isMessageClicked = false;

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                            Subject.setText("");
                            Subject.setHint("Listening...");

                        }
                    }, 1500);

                    return;
                }

                if (Message.getText().toString().equalsIgnoreCase("Swipe down to add email message...")) {
                    speak("Give me message");
                    isToClicked = false;
                    isSubjectClicked = false;
                    isMessageClicked = true;

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                            Message.setText("");
                            Message.setHint("Listening...");

                        }
                    }, 1500);
                    return;
                }

                if(!To.getText().toString().equalsIgnoreCase("Swipe down to add email address...")
                && !Subject.getText().toString().equalsIgnoreCase("Swipe down to add email subject...")
                && !Message.getText().toString().equalsIgnoreCase("Swipe down to add email message..."))
                {
                    speak("say yes no or cancel to proceed");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                            i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                            i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                            i.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say something");

                            try {
                                startActivityForResult(i, 100);
                            } catch (ActivityNotFoundException a) {
                                Toast.makeText(getApplicationContext(), "Your device doesn't support Speech Recognition", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 3000);
                }


            }
        });

    }

    private void speak(String text) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }

    }


    @Override
    public void onDestroy() {

        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        isToClicked = false;
        isSubjectClicked = false;
        isMessageClicked = false;
        IsInitialVoiceFinshed = false;
        super.onDestroy();

    }


    //Metod to ask runtime mainfist permission from user
    private void checkPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                finish();
            }
        }

    }


    @Override
    protected void onResume() {

        super.onResume();
        mGoogleApiClient.connect();
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());

    }

    /* set action bar*/
    private void setActionBar() {

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.baseline_arrow_back_white_36);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3F51B5")));
            actionBar.setIcon(new ColorDrawable(Color.parseColor("#00000000")));
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = inflater.inflate(R.layout.custom_layout_header, null);
            name = (TextView) v.findViewById(R.id.titleheadertext);
            name.setText("Compose Email");
            actionBar.setCustomView(v);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && IsInitialVoiceFinshed) {
            IsInitialVoiceFinshed = false;
            if (resultCode == RESULT_OK && null != data) {

                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (result.get(0).equals("cancel")) {
                    speak("Cancelled! You can edit your email details.");
                    To.setText("Swipe down to add email address...");
                    Subject.setText("Swipe down to add email subject...");
                    Message.setText("Swipe down to add email message...");

                } else if (result.get(0).equals("yes")) {

                    speak("Sending the mail");
                    sendEmail();

                } else if (result.get(0).equals("no")) {

                    speak("Email not sent.");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 1500);

                }

            }

            IsInitialVoiceFinshed = true;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                hideKeyBoard();
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    /* hideKeyBoard */
    private void hideKeyBoard() {

        InputMethodManager im = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    //Method to send email on user speech action when got YES
    public void sendEmail() {

        triggerSendEmail();
    }

    /**
     * Trigger the SendEmailTask if all fields are okay
     */
    private void triggerSendEmail() {

        if (!To.getText().toString().trim().isEmpty()
                && !Subject.getText().toString().trim().isEmpty()

                || !To.getText().toString().trim().isEmpty()
                && !Message.getText().toString().trim().isEmpty()) {
            if (mUtils.isValidEmail(To.getText().toString().trim())) {
                if (!isSending)

                    try {
                        new SendEmailTask(
                                createEmail(
                                        To.getText().toString().trim(),
                                        mCredential.getSelectedAccountName(),
                                        Subject.getText().toString().trim(),
                                        Message.getText().toString().trim()
                                )
                        ).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            } else {
                speak("Please add valid email address.");
            }
        } else {
            speak("Can not proceed with empty subject or message.");
        }
    }

    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     *                             Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                ComposeEmailActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    public static MimeMessage createEmail(String to, String from, String subject,
                                          String bodyText) throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));
        email.addRecipient(javax.mail.Message.RecipientType.TO,
                new InternetAddress(to));
        email.setSubject(subject);
        email.setText(bodyText);
        return email;
    }

    /**
     * Create a Message from an email
     *
     * @param email Email to be set to raw of message
     * @return Message containing base64url encoded email.
     * @throws IOException        IOException
     * @throws MessagingException MessagingException
     */
    public static Message createMessageWithEmail(MimeMessage email)
            throws MessagingException, IOException {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        email.writeTo(bytes);
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes.toByteArray());
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }


    /**
     * Send email in background
     */
    @SuppressLint("StaticFieldLeak")
    private class SendEmailTask extends AsyncTask<Void, Void, Message> {

        MimeMessage email;
        private Exception mLastError = null;

        SendEmailTask(MimeMessage email) {
            this.email = email;
        }

        @Override
        protected Message doInBackground(Void... voids) {

            Message sentMessage = null;
            isSending = true;

            try {
                Message message = createMessageWithEmail(email);
                sentMessage = mService.users().messages().send("me", message).execute();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
            }

            return sentMessage;
        }

        @Override
        protected void onPostExecute(Message output) {

            isSending = false;

            if (output != null) {
                EmailMessages emailMessages = new EmailMessages();
                emailMessages.setEmialAddress(To.getText().toString());
                emailMessages.setEmailSubject(Subject.getText().toString());
                emailMessages.setEmailMessage(Message.getText().toString());
                emailMessages.setEmailStatus("SENT");
                databaseOpenHelper.addEmailMessage(emailMessages, DatabaseOpenHelper.insertOption);
                finish();
                Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(getApplicationContext(), "An error occurred", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onCancelled() {
            isSending = false;
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(getApplicationContext(), "An error occurred", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "An error occurred", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();

    }

    @Override
    public void onConnected(Bundle bundle) {


    }

    @Override
    public void onConnectionSuspended(int i) {

    }


}
