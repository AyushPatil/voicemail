package thegenuinegourav.voicemail.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import thegenuinegourav.voicemail.R;

/*
 * TODO:This java class is useful to display selected email details on screen.We are getting filed data from recyclerview adapter onclick of list item
 */

public class EmailDetailsActivity extends AppCompatActivity {

    TextView name;
    TextView to;
    TextView subject;
    TextView status;
    TextView message;
    Button btn_submit;
    RelativeLayout ll_main;
    TextToSpeech tts;
    SwipeRefreshLayout refreshSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_details);
        setActionBar();
        to = (TextView) findViewById(R.id.to);
        subject = (TextView) findViewById(R.id.subject);
        message = (TextView) findViewById(R.id.message);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        status = (TextView) findViewById(R.id.status);
        ll_main = (RelativeLayout) findViewById(R.id.ll_main);
        refreshSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.refreshSwipeLayout);
        status.setVisibility(View.VISIBLE);
        ll_main.setVisibility(View.VISIBLE);
        btn_submit.setVisibility(View.GONE);

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });

        //Set data to required text fields like email,subject message from clicked view to detailed view
        if (getIntent() != null) {

            to.setText(getIntent().getStringExtra("email"));
            subject.setText(getIntent().getStringExtra("subject"));
            message.setText(getIntent().getStringExtra("message"));
            status.setText(getIntent().getStringExtra("status"));

        }


        refreshSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                speak("You have sent this email to " + to.getText().toString().trim() + " with subject " + subject.getText().toString().trim() + " and message " + message.getText().toString().trim() + " on" + status.getText().toString().trim());
                refreshSwipeLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                hideKeyBoard();
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    /* hideKeyBoard */
    private void hideKeyBoard() {

        InputMethodManager im = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    /* set action bar*/
    private void setActionBar() {

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.baseline_arrow_back_white_36);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3F51B5")));
            actionBar.setIcon(new ColorDrawable(Color.parseColor("#00000000")));
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = inflater.inflate(R.layout.custom_layout_header, null);
            name = (TextView) v.findViewById(R.id.titleheadertext);
            name.setText("View Email");
            actionBar.setCustomView(v);
        }
    }

    private void speak(String text) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }

    }


    @Override
    public void onDestroy() {

        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }
}
