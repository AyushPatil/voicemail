package thegenuinegourav.voicemail.activity;

import android.Manifest;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.google.api.services.people.v1.PeopleServiceScopes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import thegenuinegourav.voicemail.R;
import thegenuinegourav.voicemail.fragments.AllMailsFragment;
import thegenuinegourav.voicemail.fragments.BinMailsFragment;
import thegenuinegourav.voicemail.fragments.SentMailsFragment;
import thegenuinegourav.voicemail.utils.CommonUtility;

/*
 * TODO:This class is useful for adding fragment view on home screen with ALL,BIN & SENT
 */
public class HomeActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    FloatingActionButton floatingActionButton;
    final int RC_API_CHECK = 100;
    String pageToken = null;
    TextView name;
    BottomNavigationView navView;
    public static final int PERMISSION_REQUEST_CODE = 200;
    AlertDialog finalAlertDialog;
    GoogleAccountCredential mCredential;
    SharedPreferences sharedPref;
    GoogleApiClient mGoogleApiClient;
    public static final String TAG = "MailBoxApp";
    public static final String[] SCOPES = {GmailScopes.MAIL_GOOGLE_COM};
    public static final String PREF_ACCOUNT_NAME = "accountName";
    Gmail mService;
    public static final int REQUEST_ACCOUNT_PICKER = 1000;
    public static final int REQUEST_AUTHORIZATION = 1001;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    public static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    private TextToSpeech tts;
    SpeechRecognizer mSpeechRecognizer;
    Intent mSpeechRecognizerIntent;
    FrameLayout ll_mail_rel;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });

        ll_mail_rel = (FrameLayout) findViewById(R.id.container);
        openAppScreenByVoiceCommand();

        setActionBar();

        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        sharedPref = getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("78195453325-fgbvls3re3j02vat9rkeht5uqq91h2o3.apps.googleusercontent.com")//12500 error TODO:Add web client id and updated google-services.json
                .requestEmail()
                .requestServerAuthCode("78195453325-fgbvls3re3j02vat9rkeht5uqq91h2o3.apps.googleusercontent.com")
                .requestScopes(new Scope(PeopleServiceScopes.CONTACTS_READONLY),
                        new Scope(PeopleServiceScopes.USERINFO_EMAIL))
                .build();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this).addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        sharedPref = HomeActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);

        String accountName = sharedPref.getString(PREF_ACCOUNT_NAME, null);
        if (accountName != null) {
            mCredential.setSelectedAccountName(accountName);
        }


        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else {
            chooseAccount();
        }

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!checkPermission()) {
                    requestPermission();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        floatingActionButton =
                (FloatingActionButton) findViewById(R.id.floating_action_button);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle the click.
                if (CommonUtility.isNetworkAvailable(HomeActivity.this)) {
                    Intent composeEmialActivity = new Intent(HomeActivity.this, ComposeEmailActivity.class);
                    startActivity(composeEmialActivity);
                } else {
                    Toast.makeText(HomeActivity.this, "No internet connection!", Toast.LENGTH_LONG).show();
                }
            }
        });

        openFragment(AllMailsFragment.newInstance("", ""));
        speak("You are in a all mails.");

        navView = findViewById(R.id.bottom_navigation);

        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_all_mails:
                        openFragment(AllMailsFragment.newInstance("", ""));
                        name.setText("All");
                        speak("You are in a all mails.");
                        return true;
                    case R.id.navigation_sent_mails:
                        openFragment(SentMailsFragment.newInstance("", ""));
                        name.setText("Sent");
                        speak("You are in a sent mails.");
                        return true;
                    case R.id.navigation_bin_mails:
                        openFragment(BinMailsFragment.newInstance("", ""));
                        name.setText("Bin");
                        speak("You are in a bin mails.");
                        return true;
                }
                return false;
            }
        });

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!checkPermission()) {
                    requestPermission();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /* set action bar*/
    private void setActionBar() {

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3F51B5")));
            actionBar.setIcon(new ColorDrawable(Color.parseColor("#00000000")));
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View v = inflater.inflate(R.layout.custom_layout_header, null);
            name = (TextView) v.findViewById(R.id.titleheadertext);
            name.setText("All");

            actionBar.setCustomView(v);
        }
    }


    /**
     * Method for request runtime permission
     */
    private boolean checkPermission() {

        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        return result1 == PackageManager.PERMISSION_GRANTED;

    }

    /*
     * Method for asking runtime app permissions
     */
    private void requestPermission() {

        ActivityCompat.requestPermissions(HomeActivity.this, new String[]{

                        Manifest.permission.RECORD_AUDIO

                },
                PERMISSION_REQUEST_CODE
        );

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean one = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                            showAlertPermission();
                            return;
                        }
                    }

                    break;
                }
        }
    }

    /*
     * Method for showing permission deny alert to user
     */
    public void showAlertPermission() {

        TextView mainText;
        TextView subText;
        Button btn_ok;

        LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
        View dialogView = inflater.inflate(R.layout.custom_message_alert, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setView(dialogView);
        mainText = (TextView) dialogView.findViewById(R.id.tv_title);
        subText = (TextView) dialogView.findViewById(R.id.tv_subtext);
        btn_ok = (Button) dialogView.findViewById(R.id.btn_ok);
        finalAlertDialog = builder.create();
        finalAlertDialog.setCancelable(false);
        finalAlertDialog.setCanceledOnTouchOutside(false);
        finalAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finalAlertDialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });

        finalAlertDialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REQUEST_GOOGLE_PLAY_SERVICES:

                if (resultCode != RESULT_OK) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                    builder.setMessage(R.string.this_app_requires_google_play_services);
                    builder.setPositiveButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ActivityCompat.finishAffinity(HomeActivity.this);
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    chooseAccount();
                }
                break;

           case REQUEST_ACCOUNT_PICKER:

                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.commit();
                        mCredential.setSelectedAccountName(accountName);
                        System.out.println("ACCOUNT NAME----->>>>>" + data.getExtras().toString());
                        openFragment(AllMailsFragment.newInstance("", ""));
                        speak("You are in a all mails.");
                        callOnActivityResult();
                        new GetEmailsTask(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        chooseAccount();
                    }
                } else {
                    chooseAccount();
                }
                break;

            case REQUEST_AUTHORIZATION:

                if (resultCode == RESULT_OK) {
                    chooseAccount();
                }
                break;
        }
    }


    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.GET_ACCOUNTS)) {
            // Start a dialog from which the user can choose an account
            startActivityForResult(
                    mCredential.newChooseAccountIntent(),
                    REQUEST_ACCOUNT_PICKER);
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }


    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {

        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     *                             Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                HomeActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    @Override
    protected void onResume() {

        mGoogleApiClient.connect();
        super.onResume();

        sharedPref = getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        String accountName = sharedPref.getString(PREF_ACCOUNT_NAME, null);
        if (accountName != null) {
            callOnActivityResult();
            new GetEmailsTask(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        openAppScreenByVoiceCommand();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();

    }

    @Override
    public void onConnected(Bundle bundle) {


    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    public void callOnActivityResult() {

        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        mService = null;
        sharedPref = HomeActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);

        String accountName = sharedPref.getString(PREF_ACCOUNT_NAME, null);

        if (accountName != null) {
            mCredential.setSelectedAccountName(accountName);

            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.gmail.Gmail.Builder(
                    transport, jsonFactory, mCredential)
                    .setApplicationName("Voicemail")
                    .build();

        }
    }

    /**
     * Get emails in the background
     */
    @SuppressLint("StaticFieldLeak")
    private class GetEmailsTask extends AsyncTask<Void, Void, List<Message>> {

        private int itemCount = 0;
        private boolean clear;
        private Exception mLastError = null;

        GetEmailsTask(boolean clear) {
            this.clear = clear;
        }

        @Override
        protected List<Message> doInBackground(Void... voids) {

            List<Message> messageListReceived = null;

            try {
                String user = "me";
                String query = "in:inbox";
                ListMessagesResponse messageResponse = mService.users().messages().list(user).setQ(query).setMaxResults(20L).setPageToken(HomeActivity.this.pageToken).execute();
                HomeActivity.this.pageToken = messageResponse.getNextPageToken();

                messageListReceived = new ArrayList<>();
                List<com.google.api.services.gmail.model.Message> receivedMessages = messageResponse.getMessages();
                for (com.google.api.services.gmail.model.Message message : receivedMessages) {
                    com.google.api.services.gmail.model.Message actualMessage = mService.users().messages().get(user, message.getId()).execute();

                    Map<String, String> headers = new HashMap<>();
                    for (MessagePartHeader messagePartHeader : actualMessage.getPayload().getHeaders())
                        headers.put(
                                messagePartHeader.getName(), messagePartHeader.getValue()

                        );

                    itemCount++;
                }
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
            }

            return messageListReceived;
        }

        @Override
        protected void onPostExecute(List<Message> output) {
        }


        @Override
        protected void onCancelled() {

            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            REQUEST_AUTHORIZATION);
                }
            }
        }

    }


    public void openAppScreenByVoiceCommand() {

        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(HomeActivity.this);

        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());

        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {
                //getting all the matches
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                //displaying the first match
                if (matches != null) {
                    if (matches.get(0).matches("all")
                            || matches.get(0).equals("all")
                            || matches.get(0).equals("all mail")
                            || matches.get(0).equals("all mails")
                            || matches.get(0).equals("open all mail")
                            || matches.get(0).matches("open all mails")) {

                        openFragment(AllMailsFragment.newInstance("", ""));
                        speak("You are in a all mails.");
                        name.setText("All");

                    }

                }

                if (matches != null) {
                    if (matches.get(0).matches("sent")
                            || matches.get(0).equals("sent")
                            || matches.get(0).matches("saint")
                            || matches.get(0).equals("open sent mail")
                            || matches.get(0).equals("centmail")
                            || matches.get(0).equals("send mail")
                            || matches.get(0).equals("sent mails")
                            || matches.get(0).equals("sent mail")
                            || matches.get(0).matches("open send mails")
                            || matches.get(0).matches("open sent mails")
                            || matches.get(0).matches("open saint mails")
                            || matches.get(0).matches("open paint mails")
                            || matches.get(0).matches("paint")
                            || matches.get(0).matches("send")) {

                        openFragment(SentMailsFragment.newInstance("", ""));
                        speak("You are in a sent mails.");
                        name.setText("Sent");

                    }
                }

                if (matches != null) {
                    if (matches.get(0).matches("Bin")
                            || matches.get(0).equals("Bin")
                            || matches.get(0).equals("Bin mails")
                            || matches.get(0).equals("Bin mail")
                            || matches.get(0).matches("open Bin mails")
                            || matches.get(0).matches("open Din mails")
                            || matches.get(0).equals("open pin meals")
                            || matches.get(0).equals("open bin mail")
                            || matches.get(0).matches("open Bean mails")
                            || matches.get(0).matches("open Been mails")
                            || matches.get(0).matches("open dean mails")
                            || matches.get(0).matches("open pin meals")
                            || matches.get(0).matches("Bean")
                            || matches.get(0).matches("Din")
                            || matches.get(0).matches("deen")
                            || matches.get(0).matches("Deen")
                            || matches.get(0).matches("dean")
                            || matches.get(0).matches("Dean")) {
                        openFragment(BinMailsFragment.newInstance("", ""));
                        speak("You are in a bin mails.");
                        name.setText("Bin");
                    }

                }
                if (matches != null) {
                    if (matches.get(0).matches("Compose Email")
                            || matches.get(0).equals("Compose Email")
                            || matches.get(0).matches("Create Email")
                            || matches.get(0).equals("Create Email")
                            || matches.get(0).matches("compose email")) {

                        if (CommonUtility.isNetworkAvailable(HomeActivity.this)) {
                            Intent composeEmialActivity = new Intent(HomeActivity.this, ComposeEmailActivity.class);
                            startActivity(composeEmialActivity);
                        } else {
                            Toast.makeText(HomeActivity.this, "No internet connection!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void speak(String text) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }

    }


    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }
}
