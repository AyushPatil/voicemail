package thegenuinegourav.voicemail.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kevalpatel2106.fingerprintdialog.AuthenticationCallback;
import com.kevalpatel2106.fingerprintdialog.FingerprintDialogBuilder;
import com.kevalpatel2106.fingerprintdialog.FingerprintUtils;

import thegenuinegourav.voicemail.R;

/*
 * TODO:This java class is useful to ask user with fingerprint validation on app start
 */

public class LoginActivity extends AppCompatActivity {

    Button authenticate_btn;
    FingerprintDialogBuilder dialogBuilder;
    AuthenticationCallback callback;
    ActionBar m_myActionBar;
    public static final int PERMISSION_REQUEST_CODE = 200;
    AlertDialog finalAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //TODO:On successful login navigate to HomeActivity

        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!checkPermission()) {
                    requestPermission();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * Method to set actionbar and activity window full screen
         */
        m_myActionBar = getSupportActionBar(); // to get activity actionbar
        //For hiding actionbar
        m_myActionBar.hide();

        if (Build.VERSION.SDK_INT >= 23) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }


        //Method declarations for the fingerprint authentications callbacks
        authenticate_btn = (Button) findViewById(R.id.authenticate_btn);

        dialogBuilder = new FingerprintDialogBuilder(getApplicationContext())
                .setTitle(getResources().getString(R.string.fingerprint_dialog_title))
                .setSubtitle(getResources().getString(R.string.fingerprint_dialog_subtitle))
                .setDescription(getResources().getString(R.string.fingerprint_dialog_description))
                .setNegativeButton(getResources().getString(R.string.fingerprint_dialog_button_title));


        callback = new AuthenticationCallback() {
            @Override
            public void fingerprintAuthenticationNotSupported() {
                // Device doesn't support fingerprint authentication. May be device doesn't have fingerprint hardware or device is running on Android below Marshmallow.
                // Switch to alternate authentication method.
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_fingerprints_not_supported), Toast.LENGTH_LONG).show();
            }

            @Override
            public void hasNoFingerprintEnrolled() {
                // User has no fingerprint enrolled.
                // Application should redirect the user to the lock screen settings.
                //FingerprintUtils.openSecuritySettings(LoginActivity.this);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_no_fingerprints_enrolled), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationError(final int errorCode, @Nullable final CharSequence errString) {
                // Unrecoverable error. Cannot use fingerprint scanner. Library will stop scanning for the fingerprint after this callback.
                // Switch to alternate authentication method.
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.authentication_failed), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationHelp(final int helpCode, @Nullable final CharSequence helpString) {
                // Authentication process has some warning. such as "Sensor dirty, please clean it."
                // Handle it if you want. Library will continue scanning for the fingerprint after this callback.
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.fingerprint_dialog_description), Toast.LENGTH_LONG).show();
            }

            @Override
            public void authenticationCanceledByUser() {
                // User canceled the authentication by tapping on the cancel button (which is at the bottom of the dialog).
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_auth_canceled_by_user), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationSucceeded() {
                // Authentication success
                // Your user is now authenticated.
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.authentication_successful), Toast.LENGTH_LONG).show();
                Intent homeActivityIntent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(homeActivityIntent);
                finish();
            }

            @Override
            public void onAuthenticationFailed() {
                // Authentication failed.
                // Library will continue scanning the fingerprint after this callback.
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.authentication_failed), Toast.LENGTH_LONG).show();
            }
        };

        authenticate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.show(getSupportFragmentManager(), callback);
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!checkPermission()) {
                    requestPermission();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        dialogBuilder.show(getSupportFragmentManager(), callback);
    }


    /*
     * Method for asking runtime app permissions
     */
    private void requestPermission() {

        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{

                        Manifest.permission.RECORD_AUDIO

                },
                PERMISSION_REQUEST_CODE
        );

    }

    /**
     * Method for request runtime permission
     */
    private boolean checkPermission() {

        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        return result1 == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean one = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                            showAlertPermission();
                            return;
                        }
                    }

                    break;
                }
        }
    }

    /*
     * Method for showing permission deny alert to user
     */
    public void showAlertPermission() {

        TextView mainText;
        TextView subText;
        Button btn_ok;

        LayoutInflater inflater = LayoutInflater.from(LoginActivity.this);
        View dialogView = inflater.inflate(R.layout.custom_message_alert, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setView(dialogView);

        mainText = (TextView) dialogView.findViewById(R.id.tv_title);
        subText = (TextView) dialogView.findViewById(R.id.tv_subtext);
        btn_ok = (Button) dialogView.findViewById(R.id.btn_ok);

        finalAlertDialog = builder.create();
        finalAlertDialog.setCancelable(false);
        finalAlertDialog.setCanceledOnTouchOutside(false);

        finalAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finalAlertDialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });

        finalAlertDialog.show();

    }

}
