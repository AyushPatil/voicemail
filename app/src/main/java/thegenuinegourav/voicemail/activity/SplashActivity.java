package thegenuinegourav.voicemail.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import thegenuinegourav.voicemail.R;


/*
 * TODO:This java class is useful to load splash and app logo for 2 seconds.And it will redirectr to login screen after 2 seconds.
 */

public class SplashActivity extends AppCompatActivity {

    ActionBar m_myActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        /**
         * Method to set actionbar and activity window full screen
         */
        m_myActionBar = getSupportActionBar(); // to get activity actionbar
        //For hiding actionbar
        m_myActionBar.hide();

        if (Build.VERSION.SDK_INT >= 23) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        //Method to load login screen after 2 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //This method will be executed once the timer is over
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 2000);
    }
}