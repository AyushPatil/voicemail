package thegenuinegourav.voicemail.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import thegenuinegourav.voicemail.R;
import thegenuinegourav.voicemail.activity.EmailDetailsActivity;
import thegenuinegourav.voicemail.pojo.EmailMessages;
import thegenuinegourav.voicemail.utils.CommonUtility;


/*
 * //TODO:This java class is useful to all emails to recyclerview from database with status ALL
 */

public class AllMailsRecyclerAdapter extends RecyclerView.Adapter<AllMailsRecyclerAdapter.MyViewHolder> {

    Context context;
    private List<EmailMessages> messagesList;
    private int lastPosition;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView txtFrom;
        AppCompatTextView txtSnippet;
        AppCompatTextView txtSubject;
        AppCompatTextView txtDate;
        LinearLayoutCompat lytItemParent;

        public MyViewHolder(View view) {

            super(view);
            txtFrom = (AppCompatTextView) view.findViewById(R.id.txtFrom);
            txtSnippet = (AppCompatTextView) view.findViewById(R.id.txtSnippet);
            txtSubject = (AppCompatTextView) view.findViewById(R.id.txtSubject);
            txtDate = (AppCompatTextView) view.findViewById(R.id.txtDate);
            lytItemParent = (LinearLayoutCompat) view.findViewById(R.id.lytItemParent);


        }
    }

    public AllMailsRecyclerAdapter(Context context, List<EmailMessages> emailMessages) {
        this.context = context;
        this.messagesList = emailMessages;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;

        //Method to set data to view from arraylist
        try {
            holder.txtDate.setText(CommonUtility.convertDateFormat(messagesList.get(position).getEmailCreateOn(), "yyyy-MM-dd hh:mm:ss a", "dd/MM/yyyy"));
            holder.txtFrom.setText(messagesList.get(position).getEmialAddress());
            holder.txtSubject.setText(messagesList.get(position).getEmailSubject());
            holder.txtSnippet.setText(messagesList.get(position).getEmailMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }


        //Method to open clicked email details from list
        holder.lytItemParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent activityemails = new Intent(context, EmailDetailsActivity.class);
                    activityemails.putExtra("email", messagesList.get(position).getEmialAddress());
                    activityemails.putExtra("subject", messagesList.get(position).getEmailSubject());
                    activityemails.putExtra("message", messagesList.get(position).getEmailMessage());
                    activityemails.putExtra("status", CommonUtility.convertDateFormat(messagesList.get(position).getEmailCreateOn(), "yyyy-MM-dd hh:mm:ss a", "hh:mm a, E d, MMM yyyy"));
                    context.startActivity(activityemails);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return messagesList.size();
    }


    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {

        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();

    }

}
