package thegenuinegourav.voicemail.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.telephony.PhoneNumberUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import thegenuinegourav.voicemail.pojo.EmailMessages;
import thegenuinegourav.voicemail.utils.CommonUtility;


/*
 * //TODO:This java class is useful to create SQLite database creattion and some insert update method works.It saves data in table format with craetred columns values.
 */

public class DatabaseOpenHelper extends SQLiteOpenHelper {

    //Required variables declarations with column name,database name
    public static DatabaseOpenHelper instance;
    public static final String DATABASE_NAME = "voicemail";
    public static final String DATABASE_PATH = "/" + Environment.getExternalStorageDirectory().getAbsolutePath() + "/VoiceMail/";
    public static final int DATABASE_VERSION = 1;
    public static String insertOption = "I";
    public static String updateOption = "U";
    public static final String TABLE_EMAILS = "table_emails";
    public static final String KEY_LOCAL_ID = "key_local_id";
    public static final String KEY_EMAIL_ADDRESS = "key_email_address";
    public static final String KEY_EMAIL_SUBJECT = "key_email_subject";
    public static final String KEY_EMAIL_MESSAGE = "key_email_message";
    public static final String KEY_EMAIL_AUDIO_ATTACHMENT = "key_email_audio_attachment";
    public static final String KEY_EMAIL_STATUS = "key_email_status";
    public static final String KEY_EMAIL_CREATED_TIMESTAMP = "key_email_created_timestamp";
    public static final String KEY_EMAIL_UPDATED_TIMESTAMP = "key_email_updated_timestamp";


    static {
        instance = null;
    }


    public static synchronized DatabaseOpenHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseOpenHelper(context);
        }

        return instance;
    }

    public DatabaseOpenHelper(Context mContext) {
        super(mContext, DATABASE_NAME, null, DATABASE_VERSION);//Internal Database
        //super(mContext, DATABASE_PATH + DATABASE_NAME, null,DATABASE_VERSION);//External Database
    }

    //method for creating table in SQLite
    public static final String CREATE_TABLE_EMAILS = "CREATE TABLE "
            + TABLE_EMAILS + "("
            + KEY_LOCAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_EMAIL_ADDRESS + " TEXT,"
            + KEY_EMAIL_SUBJECT + " TEXT,"
            + KEY_EMAIL_MESSAGE + " TEXT,"
            + KEY_EMAIL_AUDIO_ATTACHMENT + " TEXT,"
            + KEY_EMAIL_STATUS + " TEXT,"
            + KEY_EMAIL_CREATED_TIMESTAMP + " TEXT,"
            + KEY_EMAIL_UPDATED_TIMESTAMP
            + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_EMAILS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //method for saving emails by status added by user
    public void addEmailMessage(EmailMessages message, String option) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_EMAIL_ADDRESS, message.getEmialAddress());
        values.put(KEY_EMAIL_SUBJECT, message.getEmailSubject());
        values.put(KEY_EMAIL_MESSAGE, message.getEmailMessage());
        values.put(KEY_EMAIL_STATUS, message.getEmailStatus());
        values.put(KEY_EMAIL_CREATED_TIMESTAMP, CommonUtility.getCurrentTimeStamp());
        values.put(KEY_EMAIL_UPDATED_TIMESTAMP, CommonUtility.getCurrentTimeStamp());
        db.insert(TABLE_EMAILS, null, values);
    }

    //method for updating emails status with user action sent,bin,all
    public void updateEmailMessage(EmailMessages message, String option) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_EMAIL_STATUS, message.getEmailStatus());
        db.insert(TABLE_EMAILS, null, values);
        int status = db.update(TABLE_EMAILS, values,
                KEY_LOCAL_ID + " = ?",
                new String[]{message.getEmailLocalID()});
    }

    //method for getting emails by status
    public List<EmailMessages> getEmailsByStatus(String status) {

        List<EmailMessages> emailMessagesList = new ArrayList<EmailMessages>();
        String selectQuery = "SELECT  * FROM " + TABLE_EMAILS
                + " WHERE " + KEY_EMAIL_STATUS + "='" + status + "' ORDER BY "
                + KEY_LOCAL_ID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                EmailMessages emailMessages = new EmailMessages();
                emailMessages.setEmailLocalID(cursor.getString(cursor
                        .getColumnIndex(KEY_LOCAL_ID)));
                emailMessages.setEmialAddress(cursor.getString(cursor
                        .getColumnIndex(KEY_EMAIL_ADDRESS)));
                emailMessages.setEmailSubject(cursor.getString(cursor
                        .getColumnIndex(KEY_EMAIL_SUBJECT)));
                emailMessages.setEmailMessage(cursor.getString(cursor
                        .getColumnIndex(KEY_EMAIL_MESSAGE)));
                emailMessages.setEmailStatus(cursor.getString(cursor
                        .getColumnIndex(KEY_EMAIL_STATUS)));
                emailMessages.setEmailCreateOn(cursor.getString(cursor
                        .getColumnIndex(KEY_EMAIL_CREATED_TIMESTAMP)));
                emailMessages.setEmailUpdatedOn(cursor.getString(cursor
                        .getColumnIndex(KEY_EMAIL_UPDATED_TIMESTAMP)));
                if (emailMessages.getEmialAddress() != null
                        && !emailMessages.getEmialAddress().equalsIgnoreCase("null")
                        && !emailMessages.getEmialAddress().isEmpty()) {
                    emailMessagesList.add(emailMessages);
                }

            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return emailMessagesList;
    }

    // Deleting single email
    public void deleteEmail(String emailId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EMAILS, KEY_LOCAL_ID + " = ?",
                new String[]{String.valueOf(emailId)});
        db.close();
    }
}
