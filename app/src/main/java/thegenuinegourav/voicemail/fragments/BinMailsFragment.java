package thegenuinegourav.voicemail.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import thegenuinegourav.voicemail.R;
import thegenuinegourav.voicemail.adapters.BinMailsRecyclerAdapter;
import thegenuinegourav.voicemail.adapters.SentMailsRecyclerAdapter;
import thegenuinegourav.voicemail.database.DatabaseOpenHelper;

/*
 * //TODO:This java class is useful to display bin mails with binding data from recycerview and database class
 */

public class BinMailsFragment extends Fragment {

    private RecyclerView refreshAllMessages;
    ImageView img_bin;
    private Paint p = new Paint();
    private View view;
    BinMailsRecyclerAdapter binMailsRecyclerAdapter;
    DividerItemDecoration mDividerItemDecoration;
    DatabaseOpenHelper databaseOpenHelper;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    public BinMailsFragment() {
        // Required empty public constructor
    }

    public static BinMailsFragment newInstance(String param1, String param2) {
        BinMailsFragment fragment = new BinMailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_bin_mails, container, false);
        refreshAllMessages = (RecyclerView) rootView.findViewById(R.id.listMessages);
        img_bin = (ImageView) rootView.findViewById(R.id.img_bin);

        try {

            initSwipe();
            databaseOpenHelper = DatabaseOpenHelper.getInstance(getActivity());

            //Get data for bin mails from db and set it to recyler adapter view
            if (databaseOpenHelper.getEmailsByStatus("BIN").size() > 0) {

                refreshAllMessages.setVisibility(View.VISIBLE);
                img_bin.setVisibility(View.GONE);

                binMailsRecyclerAdapter = new BinMailsRecyclerAdapter(getActivity(), databaseOpenHelper.getEmailsByStatus("BIN"));
                refreshAllMessages.setAdapter(binMailsRecyclerAdapter);
                mDividerItemDecoration = new DividerItemDecoration(refreshAllMessages.getContext(), DividerItemDecoration.VERTICAL);

                refreshAllMessages.setLayoutManager(new LinearLayoutManager(getActivity()));
                refreshAllMessages.setHasFixedSize(false);
                refreshAllMessages.setNestedScrollingEnabled(false);

                final Context context = refreshAllMessages.getContext();
                final LayoutAnimationController controller =
                        AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
                refreshAllMessages.setLayoutAnimation(controller);
                refreshAllMessages.getAdapter().notifyDataSetChanged();
                refreshAllMessages.scheduleLayoutAnimation();

            } else {
                refreshAllMessages.setVisibility(View.GONE);
                img_bin.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    //Swipe left to remove email from list
    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT /*| ItemTouchHelper.RIGHT*/) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                    if (direction == ItemTouchHelper.LEFT) {

                        databaseOpenHelper.deleteEmail(databaseOpenHelper.getEmailsByStatus("BIN").get(position).getEmailLocalID());
                        if (databaseOpenHelper.getEmailsByStatus("BIN").size() > 0) {

                            refreshAllMessages.setVisibility(View.VISIBLE);
                            img_bin.setVisibility(View.GONE);
                            binMailsRecyclerAdapter = new BinMailsRecyclerAdapter(getActivity(), databaseOpenHelper.getEmailsByStatus("BIN"));
                            refreshAllMessages.setAdapter(binMailsRecyclerAdapter);
                            mDividerItemDecoration = new DividerItemDecoration(refreshAllMessages.getContext(), DividerItemDecoration.VERTICAL);
                            //recycler_family_trusted_contacts_list_view.addItemDecoration(mDividerItemDecoration);
                            refreshAllMessages.setLayoutManager(new LinearLayoutManager(getActivity()));
                            //recycler_family_trusted_contacts_list_view.setItemAnimator(new DefaultItemAnimator());
                            refreshAllMessages.setHasFixedSize(false);
                            refreshAllMessages.setNestedScrollingEnabled(false);

                            final Context context = refreshAllMessages.getContext();
                            final LayoutAnimationController controller =
                                    AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
                            refreshAllMessages.setLayoutAnimation(controller);
                            refreshAllMessages.getAdapter().notifyDataSetChanged();
                            refreshAllMessages.scheduleLayoutAnimation();

                        } else {
                            refreshAllMessages.setVisibility(View.GONE);
                            img_bin.setVisibility(View.VISIBLE);
                        }
                    }

            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#388E3C"));//388E3C
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.baseline_delete_white_36);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else if (dX < 0) {
                        p.setColor(Color.parseColor("#D32F2F"));//D32F2F
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.baseline_delete_white_36);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(refreshAllMessages);
    }


    private void removeView() {

        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }

    }

}