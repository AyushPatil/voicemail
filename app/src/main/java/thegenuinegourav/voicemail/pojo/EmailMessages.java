package thegenuinegourav.voicemail.pojo;


/*
 * TODO:Pojo class to get varibales values from database and send it to recycler adapter at runtime.
 * TODO:This class is useful for data binding at runtime like getting email address,sent mails,bin mails from Sqlite Database
 */

public class EmailMessages {

    String emialAddress;
    String emailSubject;
    String emailMessage;
    String emailCreateOn;
    String emailUpdatedOn;
    String emailStatus;

    @Override
    public String toString() {
        return "EmailMessages{" +
                "emialAddress='" + emialAddress + '\'' +
                ", emailSubject='" + emailSubject + '\'' +
                ", emailMessage='" + emailMessage + '\'' +
                ", emailCreateOn='" + emailCreateOn + '\'' +
                ", emailUpdatedOn='" + emailUpdatedOn + '\'' +
                ", emailStatus='" + emailStatus + '\'' +
                ", emailLocalID='" + emailLocalID + '\'' +
                '}';
    }

    public String getEmailLocalID() {
        return emailLocalID;
    }

    public void setEmailLocalID(String emailLocalID) {
        this.emailLocalID = emailLocalID;
    }

    String emailLocalID;


    public String getEmialAddress() {
        return emialAddress;
    }

    public void setEmialAddress(String emialAddress) {
        this.emialAddress = emialAddress;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public String getEmailCreateOn() {
        return emailCreateOn;
    }

    public void setEmailCreateOn(String emailCreateOn) {
        this.emailCreateOn = emailCreateOn;
    }

    public String getEmailUpdatedOn() {
        return emailUpdatedOn;
    }

    public void setEmailUpdatedOn(String emailUpdatedOn) {
        this.emailUpdatedOn = emailUpdatedOn;
    }

    public String getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }

}
