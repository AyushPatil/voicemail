package thegenuinegourav.voicemail.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/*
 * TODO:This java class useful for getting timestamp values or device specific timezone values during email insertion in SQLite database
 */

public class CommonUtility {


    public static Context context;

    public CommonUtility() {

    }

    public CommonUtility(Context context) {
        this.context = context;
    }

    /*
      *******************************************************************
      Method to check network connectvity
      *****************************************************************

    */
    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static String getCurrentTimeStamp() {

        String currentTimeStamp = "";
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        currentTimeStamp = sdf1.format(new Date());

        try {
            Thread.sleep(100);
        } catch (Exception e) {

        }
        return currentTimeStamp;
    }

    public static String convertDateFormat(String date, String currentFormat, String newFormat) {

        SimpleDateFormat sdf = new SimpleDateFormat(currentFormat, Locale.US);
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat(newFormat, Locale.US);

        return formatter.format(testDate);

    }


}