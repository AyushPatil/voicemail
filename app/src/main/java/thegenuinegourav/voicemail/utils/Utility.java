package thegenuinegourav.voicemail.utils;

import android.content.Context;

import java.util.regex.Pattern;


/*
 * TODO:This java class is useful for validation of email address added by user
 */

public class Utility {

    private Context mContext;

    public Utility(Context context) {
        this.mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * Validates email addresses
     * @param email String
     * @return boolean
     */
    public boolean isValidEmail(String email) {

        final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
        return Pattern.compile(EMAIL_PATTERN).matcher(email).matches();

    }
}
